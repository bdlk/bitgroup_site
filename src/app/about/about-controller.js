'use strict';

angular.module('bitgroup')
  .controller('aboutCtrl', function ($scope, Firm, API_BASE) {
    var vm = this;

    vm.api_base = API_BASE;

    Firm.all().$promise.then(function(response){
      vm.firm = response.firms[0];
    });
    
  });
