'use strict';
/**
 * 
 */
angular.module('bitgroup')
  .controller('carrersCtrl', function ($scope, $timeout, Carrer) {
    var vm = this;

    vm.warning = "";

    vm.formData = {
      email: '',
      name: '',
      phone: ''
    }

    vm.submit = function(event, formData){

      if(!formData.$valid){
        return false;
      }

      vm.warning = "Saving the newsletter";

      Carrer.create({
        "carrer[name]": vm.formData.name, 
        "carrer[email]": vm.formData.email,
        "carrer[phone]": vm.formData.phone
      }).$promise.then(function(response){
        
        vm.warning = "Done!";
        $timeout(function(){
          vm.warning = "";

          vm.reset();

          formData.$submitted = false;
          formData.name.$touched = false;
          formData.email.$touched = false;
          formData.phone.$touched = false;
        }, 2000);
        
      });

    }

    vm.reset = function(){
      vm.formData.name = vm.formData.email = vm.formData.phone = '';
    }

    vm.reset();

    
  });
