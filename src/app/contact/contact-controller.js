'use strict';
/**
 * 
 */
angular.module('bitgroup')
  .controller('contactCtrl', function ($scope, $timeout, Contact) {
    var vm = this;

    vm.warning = "";

    vm.formData = {
      email: '',
      name: '',
      phone: '',
      message: ''
    }

    vm.submit = function(event, formData){

      if(!formData.$valid){
        return false;
      }

      vm.warning = "Saving the newsletter";

      Contact.create({
        "contact[name]": vm.formData.name, 
        "contact[email]": vm.formData.email,
        "contact[phone]": vm.formData.phone,
        "contact[message]": vm.formData.message
      }).$promise.then(function(response){
        
        vm.warning = "Done!";
        $timeout(function(){
          vm.warning = "";

          vm.reset();

          formData.$submitted = false;
          formData.name.$touched = false;
          formData.email.$touched = false;
          formData.phone.$touched = false;
          formData.message.$touched = false;
        }, 2000);
        
      });

    }

    vm.reset = function(){
      vm.formData.name = vm.formData.email = vm.formData.phone = vm.formData.message = '';
    }

    vm.reset();
  });
