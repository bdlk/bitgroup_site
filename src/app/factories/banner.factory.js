(function() {
  'use strict';

  angular
    .module('bitgroup')
    .factory('Banner', Banner);

  function Banner($resource, API_BASE) {

    return $resource( 
      API_BASE + '/api/v1/banners/:id',
      {
        id: null
      }, 
      {
        get: {
          url: API_BASE + '/api/v1/banners/:id',
          method: 'GET',
          isArray: false
        },
        all: {
          url: API_BASE + '/api/v1/banners',
          method: 'GET',
          isArray: false
        }
      }
    );

  }
})();