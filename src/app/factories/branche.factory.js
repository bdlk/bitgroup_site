(function() {
  'use strict';

  angular
    .module('bitgroup')
    .factory('Branche', Branche);

  function Branche($resource, API_BASE) {

    return $resource( 
      API_BASE + '/api/v1/branches/:id',
      {
        id: null
      }, 
      {
        get: {
          url: API_BASE + '/api/v1/branches/:id',
          method: 'GET',
          isArray: false
        },
        all: {
          url: API_BASE + '/api/v1/branches',
          method: 'GET',
          isArray: false
        }
      }
    );

  }
})();