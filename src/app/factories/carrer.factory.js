(function() {
  'use strict';

  angular
    .module('bitgroup')
    .factory('Carrer', Carrer);

  function Carrer($resource, API_BASE) {

    return $resource( 
      API_BASE + '/api/v1/carrers/:id',
      {
        id: null
      }, 
      {
        create: {
          url: API_BASE + '/api/v1/carrers',
          method: 'GET',
          isArray: false
        }
      }
    );

  }
})();