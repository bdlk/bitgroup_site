(function() {
  'use strict';

  angular
    .module('bitgroup')
    .factory('Category', Category);

  function Category($resource, API_BASE) {

    return $resource( 
      API_BASE + '/api/v1/categories/:id',
      {
        id: null
      }, 
      {
        get: {
          url: API_BASE + '/api/v1/categories/:id',
          method: 'GET',
          isArray: false
        },
        all: {
          url: API_BASE + '/api/v1/categories',
          method: 'GET',
          isArray: false
        }
      }
    );

  }
})();