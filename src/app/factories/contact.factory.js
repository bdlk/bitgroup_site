(function() {
  'use strict';

  angular
    .module('bitgroup')
    .factory('Contact', Contact);

  function Contact($resource, API_BASE) {

    return $resource( 
      API_BASE + '/api/v1/contacts/:id',
      {
        id: null
      }, 
      {
        create: {
          url: API_BASE + '/api/v1/contacts',
          method: 'GET',
          isArray: false
        }
      }
    );

  }
})();