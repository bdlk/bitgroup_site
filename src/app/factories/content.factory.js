(function() {
  'use strict';

  angular
    .module('bitgroup')
    .factory('Content', Content);

  function Content($resource, API_BASE) {

    return $resource( 
      API_BASE + '/api/v1/contents/:id',
      {
        id: null
      }, 
      {
        get: {
          url: API_BASE + '/api/v1/contents/:id',
          method: 'GET',
          isArray: false
        },
        all: {
          url: API_BASE + '/api/v1/contents',
          method: 'GET',
          isArray: false
        }
      }
    );

  }
})();