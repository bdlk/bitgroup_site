(function() {
  'use strict';

  angular
    .module('bitgroup')
    .factory('Corporation', Corporation);

  function Corporation($resource, API_BASE) {

    return $resource( 
      API_BASE + '/api/v1/corporations/:id',
      {
        id: null
      }, 
      {
        get: {
          url: API_BASE + '/api/v1/corporations/:id',
          method: 'GET',
          isArray: false
        },
        all: {
          url: API_BASE + '/api/v1/corporations',
          method: 'GET',
          isArray: false
        }
      }
    );

  }
})();