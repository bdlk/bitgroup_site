(function() {
  'use strict';

  angular
    .module('bitgroup')
    .factory('Firm', Firm);

  function Firm($resource, API_BASE) {

    return $resource( 
      API_BASE + '/api/v1/firms/:id',
      {
        id: null
      }, 
      {
        get: {
          url: API_BASE + '/api/v1/firms/:id',
          method: 'GET',
          isArray: false
        },
        all: {
          url: API_BASE + '/api/v1/firms',
          method: 'GET',
          isArray: false
        }
      }
    );

  }
})();