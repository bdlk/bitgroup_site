(function() {
  'use strict';

  angular
    .module('bitgroup')
    .factory('Newsletter', Newsletter);

  function Newsletter($resource, API_BASE) {

    return $resource( 
      API_BASE + '/api/v1/newsletters/:id',
      {
        id: null
      }, 
      {
        create: {
          url: API_BASE + '/api/v1/newsletters',
          method: 'GET',
          isArray: false
        }
      }
    );

  }
})();