(function() {
  'use strict';

  angular
    .module('bitgroup')
    .factory('Product', Product);

  function Product($resource, API_BASE) {

    return $resource( 
      API_BASE + '/api/v1/products/:id',
      {
        id: null
      }, 
      {
        get: {
          url: API_BASE + '/api/v1/products/:id',
          method: 'GET',
          isArray: false
        },
        all: {
          url: API_BASE + '/api/v1/products',
          method: 'GET',
          isArray: false
        }
      }
    );

  }
})();