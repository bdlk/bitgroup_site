(function() {
  'use strict';

  angular
    .module('bitgroup')
    .factory('Social', Social);

  function Social($resource, API_BASE) {

    return $resource( 
      API_BASE + '/api/v1/socials/:id',
      {
        id: null
      }, 
      {
        get: {
          url: API_BASE + '/api/v1/socials/:id',
          method: 'GET',
          isArray: false
        },
        all: {
          url: API_BASE + '/api/v1/socials',
          method: 'GET',
          isArray: false
        }
      }
    );

  }
})();