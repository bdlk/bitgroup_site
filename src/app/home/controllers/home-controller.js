'use strict';
/**
 * @ngdoc function
 * @name bitgroup.controller:homeCtrl
 * @description
 * # homeCtrl
 * Controller of the bitgroup
 */
angular.module('bitgroup')
  .controller('homeCtrl', function ($scope, $timeout, Banner, Corporation, Category, Firm, Social, Product, Content, API_BASE) {
    var vm = this;

    vm.api_base = API_BASE;

    Banner.all().$promise.then(function(response){
      vm.banners = response.banners;

      $timeout(function(){
        $('.banner-body').slick({
          autoplay: true,
          autoplaySpeed: 4000
        });

        vm.next = function(){
          $('.banner-body').slick('slickNext');
        }

        vm.prev = function(){
          $('.banner-body').slick('slickPrev');
        }
      }, 1000);
      
    });

    Corporation.all().$promise.then(function(response){
      vm.corporations = response.corporations;
    });

    Category.all().$promise.then(function(response){
      vm.categories = response.categories;
    });

    Firm.all().$promise.then(function(response){
      vm.firm = response.firms[0];
    });

    Social.all().$promise.then(function(response){
      vm.socials = response.socials;
    });

    Product.all().$promise.then(function(response){
      vm.products = response.products;

      $timeout(function(){
        $('.product-slick').slick({
          slidesToShow: 3, 
          slidesToScroll: 3,
          arrows: false,
          autoplay: true,
          autoplaySpeed: 4000,
          responsive: [
          {
            breakpoint: 992,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
        });

        vm.product_next = function(){
          $('.product-slick').slick('slickNext');
        }

        vm.product_prev = function(){
          $('.product-slick').slick('slickPrev');
        }
      }, 1000);
      
      
    });

    Content.all().$promise.then(function(response){
      vm.contents = response.contents;
    });

    
    
  });
