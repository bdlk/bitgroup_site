/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('bitgroup')
    .constant('malarkey', malarkey)
    .constant('moment', moment)
    .constant('API_BASE', "http://api.bitgroup.co.za");
    //.constant('API_BASE', "http://localhost:3000");

})();
