(function() {
  'use strict';

  angular
    .module('bitgroup')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('main', {
        abstract: true,
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .state('main.home', {
        url: '/',
        templateUrl: 'app/home/views/home.html',
        controller: 'homeCtrl',
        controllerAs: 'home'
      })
      .state('main.about', {
        url: '/about-us',
        templateUrl: 'app/about/about.html',
        controller: 'aboutCtrl',
        controllerAs: 'about'
      })
      .state('main.branches', {
        url: '/branches',
        templateUrl: 'app/branches/branches.html',
        controller: 'branchesCtrl',
        controllerAs: 'branches'
      })
      .state('main.products', {
        url: '/products',
        templateUrl: 'app/products/products.html',
        controller: 'productsCtrl',
        controllerAs: 'products'
      })
      .state('main.products_inside', {
        url: '/products-inside/:id',
        templateUrl: 'app/products/products_inside.html',
        controller: 'productsInsideCtrl',
        controllerAs: 'products_inside'
      })
      .state('main.contact', {
        url: '/contact',
        templateUrl: 'app/contact/contact.html',
        controller: 'contactCtrl',
        controllerAs: 'contact'
      })
      .state('main.carrers', {
        url: '/carrers',
        templateUrl: 'app/carrers/carrers.html',
        controller: 'carrersCtrl',
        controllerAs: 'carrer'
      });

    $urlRouterProvider.otherwise('/');
  }

})();
