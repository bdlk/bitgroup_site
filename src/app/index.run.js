(function() {
  'use strict';

  angular
    .module('bitgroup')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
