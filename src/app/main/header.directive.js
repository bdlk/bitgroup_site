/**
* Login directive
**/

(function () {

  'use strict';

  angular
  .module( 'bitgroup' )
  .directive('ngHeader', ngHeader);

  function ngHeader($rootScope) {
    return {
      restrict: 'A',
      link: function (scope, element) {
        
        scope.checked = false;

        scope.toggle = function(){
          scope.checked = true;
        }

        scope.close = function(){
          scope.checked = false;
        }
      }
    };
  };

}());