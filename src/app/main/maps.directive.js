/**
* Login directive
**/

(function () {

  'use strict';

  angular
  .module( 'bitgroup' )
  .directive('bitMaps', bitMaps);

  function bitMaps($rootScope, Branche) {
    return {
      restrict: 'A',
      link: function (scope, element) {
        
        var mapDiv = document.getElementById("map");
        var markerList = [];

        scope.addMap = function (latitude, longitude) {
          
          var cords = new google.maps.LatLng(latitude, longitude);

          var mapOptions = {
            center: cords,
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false
          };

          var map = new google.maps.Map(mapDiv, mapOptions);

          var image = {
            url: '/assets/images/marker.png',
            scaledSize: new google.maps.Size(30, 45)
          };

          function add_marker(latitude, longitude, index){
            var marker;

            var cords = new google.maps.LatLng(latitude, longitude);

            marker = new google.maps.Marker({
              position: cords,
              map: map,
              title: '',
              icon: image
            });

            google.maps.event.addListener(marker, 'click', function () {
              map.setCenter(this.getPosition());
            });

            marker.setMap(map);

            markerList[index] = marker;

          }

          angular.forEach(scope.branches, function(info) {
            var latitude = info.latitude;
            var longitude = info.longitude;

            add_marker(latitude, longitude, info.id);
          });

        }

        Branche.all().$promise.then(function(response){
          scope.branches = response.branches;

          scope.addMap(scope.branches[0].latitude, scope.branches[0].longitude);

          scope.branche_selected = scope.branches[0];

          angular.element(".map_change").change(function(){

            setTimeout(function() {
              
              google.maps.event.trigger(markerList[scope.branche_selected.id], 'click');  

            }, 10);
            

          });

          scope.map_info = scope.branches;

          google.maps.event.trigger(markerList[scope.branche_selected.id], 'click');
          
        });

        

      }
    };
  };

}());