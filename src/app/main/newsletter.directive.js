/**
* Login directive
**/

(function () {

  'use strict';

  angular
  .module( 'bitgroup' )
  .directive('ngNewsletter', ngNewsletter);

  function ngNewsletter($rootScope, Newsletter, $timeout) {
    return {
      restrict: 'A',
      link: function (scope, element) {
        
        scope.warning = "";

        scope.formData = {
          email: '',
          name: ''
        }

        scope.submit = function(event, formData){

          if(!formData.$valid){
            return false;
          }

          scope.warning = "Saving the newsletter";

          Newsletter.create({"newsletter[name]": scope.formData.name, "newsletter[email]": scope.formData.email}).$promise.then(function(response){
            
            scope.warning = "Done!";
            $timeout(function(){
              scope.warning = "";

              scope.reset();

              formData.$submitted = false;
              formData.email.$touched = false;
            }, 2000);
            
          });

        }

        scope.reset = function(){
          scope.formData.name = scope.formData.email = '';
        }

        scope.reset();
      }
    };
  };

}());