'use strict';
/**
 * 
 */
angular.module('bitgroup')
  .controller('productsInsideCtrl', function ($scope, Category, API_BASE, $stateParams) {
    var vm = this;

    vm.api_base = API_BASE;

    Category.get({id: $stateParams.id}).$promise.then(function(response){
      vm.category = response;
    });

    
  });
