'use strict';
/**
 * 
 */
angular.module('bitgroup')
  .controller('productsCtrl', function ($scope, Category, API_BASE) {
    var vm = this;

    vm.api_base = API_BASE;
    
    Category.all().$promise.then(function(response){
      vm.categories = response.categories;
    });
    
  });
